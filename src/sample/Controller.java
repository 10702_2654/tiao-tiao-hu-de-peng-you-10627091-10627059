package sample;

import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Controller {
    public ComboBox car;
    public DatePicker calendar;
    public ComboBox driver;
    public ComboBox tire;
    public ComboBox paint;
    public Button fix;
    public Button point;
    public Button buy;
    public Label price;
    public ImageView image;
    public Button buybt;
    public ComboBox jk;
    int i=0,x=0,y=0,z=0,ans=0;
    List<String> a = new ArrayList<String>();
    List<String> b = new ArrayList<String>();
    List<String> c= new ArrayList<String>();
    List<String> d= new ArrayList<String>();
    public void initialize(){
        File file = new File("ggg.gif");

        image.setImage(new Image(file.toURI().toString()));
        a.add("T-14阿瑪塔主力戰車(5000)");
        a.add("M1A3(20000)");
        a.add("MBT-70主力戰車(6000)");
        a.add("豹2式(4000)");
        car.setItems(FXCollections.observableList(a));
        b.add("廖鴻戎(免費)");
        b.add("佑銓(2000)");
        b.add("左拳(5000)");
        b.add("右腳(6000)");
        b.add("左腳(7000)");
        driver.setItems(FXCollections.observableList(b));
        c.add("紅色");
        c.add("綠色");
        c.add("藍色");
        c.add("黃色");
        c.add("黑色");
        paint.setItems(FXCollections.observableList(c));
        d.add("要");
        d.add("不要");
        jk.setItems(FXCollections.observableList(d));
    }

    public void inck(ActionEvent actionEvent) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("確定"); //設定對話框視窗的標題列文字
        alert.setHeaderText("確定訂單嗎？"); //設定對話框視窗裡的標頭文字。若設為空字串，則表示無標頭
        final Optional<ButtonType> opt = alert.showAndWait();
        final ButtonType rtn = opt.get(); //可以直接用「alert.getResult()」來取代
        System.out.println(rtn);
        if (rtn == ButtonType.OK) {
            //若使用者按下「確定」
            final Alert alert2 = new Alert(Alert.AlertType.INFORMATION); // 實體化Alert對話框物件，並直接在建構子設定對話框的訊息類型
            alert2.setTitle("訂購成功"); //設定對話框視窗的標題列文字
            alert2.setHeaderText("車票已訂購成功"); //設定對話框視窗裡的標頭文字。若設為空字串，則表示無標頭
            alert2.showAndWait(); //顯示對話框，並等待對話框被關閉時才繼續執行之後的程式
        }
        else if (rtn == ButtonType.CANCEL){
            //若使用者按下「取消」，也可直接使用else
            final Alert alert2 = new Alert(Alert.AlertType.INFORMATION); // 實體化Alert對話框物件，並直接在建構子設定對話框的訊息類型
            alert2.setTitle("取消"); //設定對話框視窗的標題列文字
            alert2.setHeaderText("車票已取消"); //設定對話框視窗裡的標頭文字。若設為空字串，則表示無標頭
            alert2.showAndWait(); //顯示對話框，並等待對話框被關閉時才繼續執行之後的程式
        }
    }

    public void prompbt(ActionEvent actionEvent) {
        final Alert alert = new Alert(Alert.AlertType.INFORMATION); // 實體化Alert對話框物件，並直接在建構子設定對話框的訊息類型
        alert.setTitle("提示"); //設定對話框視窗的標題列文字
        alert.setHeaderText("如有意外與本公司無關"); //設定對話框視窗裡的標頭文字。若設為空字串，則表示無標頭
        alert.showAndWait(); //顯示對話框，並等待對話框被關閉時才繼續執行之後的程式
    }

    public void buyclick(ActionEvent actionEvent) {
        if(car.getValue()=="T-14阿瑪塔主力戰車(5000)")x=5000;
        if(car.getValue()=="M1A3(20000)")x=20000;
        if(car.getValue()=="MBT-70主力戰車(6000)")x=6000;
        if(car.getValue()=="豹2式(4000)")x=4000;
        if(driver.getValue()=="廖鴻戎(免費)")y=0;
        if(driver.getValue()=="佑銓(2000)")y=2000;
        if(driver.getValue()=="左拳(5000)")y=5000;
        if(driver.getValue()=="右腳(6000)")y=6000;
        if(driver.getValue()=="左腳(7000)")y=7000;
        if(jk.getValue()=="要")z=2;
        if(jk.getValue()=="不要")z=1;
        ans=x*z+y+2000;
        price.setText("價格:"+"人民幣"+ans);
    }
}
